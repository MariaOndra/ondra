﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject.SpaceFighter;

[RequireComponent(typeof(CanvasGroup))]
public class FailedScreen : MonoBehaviour
{
    [SerializeField] private PlayerFacade _player;              
    private CanvasGroup _canvasGroup;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();

        CloseScreen();
    }

    private void Update()
    {
        if (!_player.IsAlmostDead)
            CloseScreen();
        else
            ShowScreen();
    }

    public void Continue() 
    {
        _player.Model.ReturnHealth();
        _player.Model.IsAlmostDead = false;
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ShowScreen() 
    {
        Time.timeScale = 0;
        _canvasGroup.alpha = 1;
        _canvasGroup.interactable = true;
    }

    public void CloseScreen() 
    {
        Time.timeScale = 1;
        _canvasGroup.alpha = 0;
        _canvasGroup.interactable = false;
    }
}
